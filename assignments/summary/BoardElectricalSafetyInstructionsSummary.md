The Internet of things (IoT) is taking the concept of safety to a whole new level.  In a world where everything can be connected, and physical contact becomes optional, verbal instructions and even facial recognition are used to operate the end device, safety considerations must be made.

**There are two categories of safety hazards associated with the IOT:**

<h3>Type 1 hazards </h3>
They are directly associated with the traditional use of the device and include things such as: overheating, shock, sonic hazards, etc. Introducing IoT into products could increase the frequency of occurrence of these hazards. Enabling remote operations means that hazards of these kinds are no longer bounded by physical limitations. For instance, an electronic oven that can be turned on and off remotely can become hazardous if a faulty command is received when no one is present to watch it.

<h3>Type 2 hazards</h3>
They are indirectly related to the device and its operation but could enable a security or safety issue by implementing IoT. The cascade effect could be a breaching of properties or leakage of private information. Imagine a snowy winter in the northeastern U.S. where a smart garage door opener fails and starts to open while the family is away for vacation. The property is exposed to severe weather, whereas a hardwired garage opener will more likely to stay closed during a failure.
Newly developed regulations and standards address the above hazards properly by recognizing the root cause of these hazards and the unseen applications for existing products. One leading sector is home appliances. At present smart cooking, lighting and temperature control are the frontline of IoT safety candidates. Drones and similar devices with the capability of injuring people or damaging financial assets must also be considered. Autonomous vehicles are being produced widely and IoT-enabled devices for the automobile industry are emerging, bringing another layer of safety concerns to driving hazards.

It is expected that IoT safety evaluation will be closely coupled with functional safety, which was established at the end of the 20th century for industrial environments. International and regional standards include: IEC61508 series, ISO26262 series, ISO12100, ISO13849 series, IEC60730-1 series, UL5500, CSA Z434 series, ANSI B11 series.
![Safety for IoT](/extras/safety.jpg)

**Key aspects related to functional safety evaluation include:**

- Planning and design elements
- Risks and hazards
- Management plans
- Performance Levels (PLs)
- Safety Integrity Levels (SILs)
- Failure modes, effects, and diagnostic analysis (FMEDA)
- Designated personnel responsibility
- Rigorous documentation

As the IoT continues to evolved, it's important to stay informed about risk and safety standards to help make sure products meet regulatory requirements and the demands of consumers.  It could be the key to success.